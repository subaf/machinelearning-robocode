package sample;

import java.io.IOException;
import robocode.*;
import static robocode.util.Utils.normalRelativeAngleDegrees;
//

import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.*;
import java.net.http.HttpResponse;

import org.json.*;

@SuppressWarnings("Duplicates")

public class MujRobot extends AdvancedRobot {

	//	statistics
	private int scaned_counter = 0;
	private int hit_counter = 0;
	private int fire_counter = 0;
	double acc;

	//	moving
	int dist = 75;
	int moveDirection=1;//which way to move

	public void run() {
		while (true) {
			ahead(3000);
			setAdjustRadarForRobotTurn(true);//keep the radar still while we turn
			setAdjustGunForRobotTurn(true); // Keep the gun still when we turn
			turnRadarRightRadians(Double.POSITIVE_INFINITY);//keep turning radar right
		}
	}



	public void onScannedRobot(ScannedRobotEvent e) {
		scaned_counter++;
		System.out.println("Scan - " + scaned_counter + ".");

		double absBearing = e.getBearingRadians() + getHeadingRadians();//enemies absolute bearing
		double latVel = e.getVelocity() * Math.sin(e.getHeadingRadians() - absBearing);//enemies later velocity
		double gunTurnAmt;//amount to turn our gun
		setTurnRadarLeftRadians(getRadarTurnRemainingRadians());//lock on the radar
		if(Math.random() > .9){
			setMaxVelocity((12 * Math.random()) + 12);//randomly change speed
		}
		if (e.getDistance() > 150) {//if distance is greater than 150
			gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing - getGunHeadingRadians() + latVel / 22);//amount to turn our gun, lead just a little bit
			setTurnGunRightRadians(gunTurnAmt); //turn our gun
			setTurnRightRadians(robocode.util.Utils.normalRelativeAngle(absBearing - getHeadingRadians() + latVel / getVelocity()));//drive towards the enemies predicted future location
			setAhead((e.getDistance() - 140) * moveDirection);//move forward
			try {
				fireModel(e);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

		}
		else{//if we are close enough...
			gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing - getGunHeadingRadians() + latVel / 15);//amount to turn our gun, lead just a little bit
			setTurnGunRightRadians(gunTurnAmt);//turn our gun
			setTurnLeft(-90 - e.getBearing()); //turn perpendicular to the enemy
			setAhead((e.getDistance() - 140) * moveDirection);//move forward
			try {
				fireModel(e);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}

	}

	//   robot moving strategy:
	//	 We were hit!  Turn perpendicular to the bullet,
	//	 so our seesaw might avoid a future shot.
	public void onHitByBullet(HitByBulletEvent e) {
		turnRight(normalRelativeAngleDegrees(90 - (getHeading() - e.getHeading())));

		ahead(dist);
		dist *= -1;
		scan();
	}

	//	robot moving strategy:
	//	reverse direction upon hitting a wall
	public void onHitWall(HitWallEvent e){
		moveDirection=-moveDirection;
	}

	//	when round ended print statistics
	public void onRoundEnded(RoundEndedEvent event) {
		System.out.println("*****************************************************************");
		System.out.println("Game statistics");
		System.out.println("Scaned " + scaned_counter + " times");
		System.out.println("Fire " + fire_counter + " times");
		System.out.println("Hit " + hit_counter + " times");
		acc = ((double)hit_counter / (double)fire_counter);
		System.out.println("Fire accuracy (hit / fired)  = " + (acc * 100) + "%");
		System.out.println("*****************************************************************");

	}

	JSONObject json = new JSONObject();

	//  sudo python2.7 flask_server.py
	//	fire when model respond to fire and robot can fire (gun is not overheat)
	public void fireModel(ScannedRobotEvent e) throws IOException, InterruptedException {
		if (getGunHeat() == 0) {

			json.put("e.getDistance", String.valueOf(e.getDistance()));
			json.put("e.getBearing", String.valueOf(e.getBearing()));

			HttpRequest request = HttpRequest.newBuilder()
					.uri(URI.create("http://localhost:488/predict"))
					.header("Content-Type", "application/json")
					.POST(HttpRequest.BodyPublishers.ofString(json.toString()))
					.build();

			HttpClient httpClient = HttpClient.newBuilder()
					.version(HttpClient.Version.HTTP_2)  // this is the default
					.build();

			HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());


			System.out.println("ODPOVED SERVERA : " + response.body());

			if(response.body().contains("1")) {
				fire(3);
				//	send to server: e.getBearing() and e.getDistance()
				fire_counter++;
			}
		}
	}


	//	just for statistic, count hits
	public void onBulletHit(BulletHitEvent event) {
		hit_counter++;
	}
}