#!/usr/bin/env python

import flask
import pickle
from flask import Flask, request, jsonify

app = flask.Flask(__name__)

intro_file = open("intro.html","r")
intro = intro_file.read()
##getting our trained model from a file we created earlier
model = pickle.load(open("model.pkl","r"))

@app.route('/')
def main_page():
    return intro

@app.route('/test', methods=['POST'])
def test_page():
    test = request.get_json()['test']
    return "Tested input from JSON: " + test

@app.route('/predict', methods=['POST'])
def predict():
    try:
        data = request.get_json()
        value_distance = float(data["e.getDistance"])
        value_bearing = float(data["e.getBearing"])
        values = [value_distance, value_bearing]
    except ValueError:
        return jsonify("Please enter a JSON format.")

    return jsonify(model.predict([values]).tolist())

if __name__ == '__main__':
    app.run(port=488)
