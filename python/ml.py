#!/usr/bin/env python

"""
Example for understanding...
https://www.dataquest.io/blog/machine-learning-python/
"""

# Import the pandas library.
import pandas

# Read in the data.
target = pandas.read_csv("target.csv")
# Print the names of the columns in target.
print(target.columns)
print(target.shape)


""" Hit statictics
# Import matplotlib
import matplotlib.pyplot as plt
# Make a histogram of all the ratings in the average_rating column.
plt.hist(target["Hit"])
plt.bar(["True","False"],0)
plt.title("Hit statictics")
plt.xlabel("Hit")
plt.ylabel("Bullets")
# Show the plot.
plt.show()
"""

target[target["Hit"] == 0]
# Print the first row of all the target with zero scores.
# The .iloc method on dataframes allows us to index by position.
#print(target[target["Hit"] == 0].iloc[0])
# Print the first row of all the target with scores greater than 0.
#print(target[target["Hit"] > 0].iloc[0])
# Remove any rows with missing values.
target = target.dropna(axis=0)

""" only testing purpose
# Import the kmeans clustering model.
from sklearn.cluster import KMeans
#https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
# Initialize the model with 2 parameters -- number of clusters and random state.
kmeans_model = KMeans(n_clusters=5, random_state=1)
# Get only the numeric columns from target.
good_columns = target._get_numeric_data()
# *** How to add binary format??? True/False
# Fit the model using the good columns.
kmeans_model.fit(good_columns)
# Get the cluster assignments.
labels = kmeans_model.labels_

# Import the PCA model.
from sklearn.decomposition import PCA
# Create a PCA model.
pca_2 = PCA(2)
# Fit the PCA model on the numeric columns from earlier.
plot_columns = pca_2.fit_transform(good_columns)
# Make a scatter plot of each game, shaded according to cluster assignment.
plt.scatter(x=plot_columns[:,0], y=plot_columns[:,1], c=labels)
# Show the plot.
plt.show()
"""

# Finding correlations
print("******* Correlations **********")
print(target.corr()["Hit"])

# Get all the columns from the dataframe.
columns = target.columns.tolist()
# Filter the columns to remove ones we don't want.
columns = [c for c in columns if c not in ["getX","getY","e.getHeading",\
                                            "getGunHeading","Hit"]]
print("Data used for machine learning: ", columns)


# Store the variable we'll be predicting on.
to_predict = "Hit"
# Import a convenience function to split the sets.
#from sklearn.cross_validation import train_test_split
from sklearn.model_selection import train_test_split
# Generate the training set.  Set random_state to be able to replicate results.
train = target.sample(frac=0.8, random_state=1)
# Select anything not in the training set and put it in the testing set.
test = target.loc[~target.index.isin(train.index)]
# Print the shapes of both sets.
print("******** Shapes ********")
print("Train shape: ", train.shape)
print("Test shape: ", test.shape)


# Import the scikit-learn function to compute error.
from sklearn.metrics import mean_squared_error
from sklearn.neighbors import KNeighborsClassifier

""" Graph of neighbors progress
import numpy as np # linear algebra
# Try different numbers of n_estimators - this will take a minute or so
neighbors = np.arange(1, 37, 1)
scores = []
for n in neighbors:
    model = KNeighborsClassifier(n_neighbors=n)
    model.fit(train[columns], train[to_predict])
    scores.append(model.score(test[columns], test[to_predict]))
plt.title("Effect of n_neighbors")
plt.xlabel("n_neighbors")
plt.ylabel("score")
plt.plot(neighbors, scores)
plt.axis([1, 37, 0.55, 0.7])
plt.show()
"""


print("---------- KNeighborsClassifier ------------")
model = KNeighborsClassifier(n_neighbors=37)
model.fit(train[columns], train[to_predict])
print("SCORE:", model.score(test[columns], test[to_predict]))
predictions = model.predict(test[columns])
# Compute error between our test predictions and the actual values.
print("ERROR: ", mean_squared_error(predictions, test[to_predict]))

print(predictions.tolist())


import pickle
#serializing our model to a file called model.pkl
pickle.dump(model, open("model.pkl","wb"))
